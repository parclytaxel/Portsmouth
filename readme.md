This repository contains source code for my small personal website, which can be accessed at [parclytaxel.art](https://parclytaxel.art).

In keeping with my tradition of naming repositories after places, I chose [Portsmouth](https://en.wikipedia.org/wiki/Portsmouth) since the website's purpose is to communicate information about me to the world, like a maritime port.
